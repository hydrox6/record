import mss
import os
from PyQt5.QtCore import (QPoint)
from PyQt5.QtGui import (QScreen)
import sys
from typing import List, Tuple

import constants


def get_resource_path(path: str) -> str:
    # When in OneFile mode, PyInstaller extracts and runs from a temp folder stored in this var
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, path)
    else:
        return os.path.join(constants.INSTALL_DIRECTORY, path)


def get_current_screen(cursor: QPoint, screens: List[QScreen]) -> Tuple[int, QScreen or None]:
    for i, s in enumerate(screens):
        if s.availableGeometry().contains(cursor):
            return i + 1, s
    return -1, None


def capture_screen(screen_index=-1) -> None:
    with mss.mss() as sct:
        sct.shot(mon=screen_index, output=constants.PREVIEW_CAPTURE_PATH)


def getXYWH(start: QPoint, end: QPoint) -> Tuple[int, int, int, int]:
    return min(start.x(), end.x()), min(start.y(), end.y()), abs(end.x() - start.x()), abs(end.y() - start.y())


def getRect(start: QPoint, end: QPoint) -> Tuple[int, int, int, int]:
    return min(start.x(), end.x()), min(start.y(), end.y()), max(start.x(), end.x()), max(start.y(), end.y())
