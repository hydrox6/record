#!/usr/bin/env bash
rm -rf ~/.local/share/record
rm ~/.local/bin/record
mv -v ./dist/* ~/.local/share/
ln -s ~/.local/share/record/record ~/.local/bin/record
rm -rf ./build
rm -rf ./dist