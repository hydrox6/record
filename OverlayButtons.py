from PyQt5.QtCore import (Qt)
from PyQt5.QtGui import (QFont, QIcon, QPainter, QPaintEvent)
from PyQt5.QtWidgets import (QApplication, QPushButton, QSystemTrayIcon, QWidget, QMessageBox)
import time

import constants
import util


class OverlayButtons(QWidget):

    def __init__(self, x: int, y: int):
        # Qt setup
        super().__init__()
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.Tool)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowTitle("Recorder buttons")

        self.readout_size = (70, 25)
        self.font = QFont("bogus")
        self.font.setStyleHint(QFont.Monospace)
        self.startTime = time.time() + constants.PRE_START_DELAY
        self.isEncoding = False
        self.cancelled = False
        self.capture_running = True
        self.button_width = 50
        self.button_count = 2
        self.button_padding = 5
        self.size = (self.readout_size[0] + (self.button_width + self.button_padding) * self.button_count, self.readout_size[1])

        # Recording tray icon setup
        self.trayIcon = QSystemTrayIcon(self)
        self.trayIcon.setToolTip("Recording")
        self.trayIcon.setIcon(QIcon(util.get_resource_path("recording.png")))
        self.trayIcon.activated.connect(lambda reason, overlay=self: overlay.set_encoding())
        self.trayIcon.show()

        # Add buttons
        bx, h = self.readout_size
        bx += self.button_padding - 1
        end = QPushButton("End", self)
        end.clicked.connect(lambda _, overlay=self: overlay.set_encoding())
        end.move(bx + 1, 0)
        end.setFixedSize(self.button_width, h)
        end.setStyleSheet("background-color: rgb(0, 230, 0);")
        end.show()
        bx += self.button_width + self.button_padding
        cancel = QPushButton("Cancel", self)
        cancel.clicked.connect(lambda _, overlay=self: overlay.cancel())
        cancel.move(bx + 1, 0)
        cancel.setFixedSize(self.button_width, h)
        cancel.setStyleSheet("background-color: rgb(230, 0, 0);")
        cancel.show()

        self.setGeometry(x - 1, y + 3, *self.size)

    def paintEvent(self, e: QPaintEvent):
        c = QPainter()
        c.begin(self)
        c.setRenderHint(QPainter.Antialiasing, True)

        # Draw readout
        c.setPen(Qt.black)
        c.setBrush(Qt.lightGray)
        c.setFont(self.font)
        if self.isEncoding:
            c.drawRect(0, 0, *self.readout_size)
            c.drawText(7, 17, "Encoding")
        else:
            rawdur = time.time() - self.startTime
            dur = abs(int(rawdur))
            min = str(dur // 60).rjust(2, "0")
            sec = str(dur % 60).rjust(2, "0")
            hun = str(rawdur % 1)[2:4]
            if rawdur < 0:
                c.setBrush(Qt.red)
            c.drawRect(0, 0, *self.readout_size)
            c.drawText(7, 17, f"{min}:{sec}.{hun}")
        c.end()

    def set_encoding(self):
        self.capture_running = False
        # Change name of tray icon
        # Not deleting it causes the information to not actually change, for some reason
        self.trayIcon.deleteLater()
        self.trayIcon = QSystemTrayIcon(self)
        self.trayIcon.setToolTip("Encoding")
        self.trayIcon.setIcon(QIcon(util.get_resource_path("encoding.png")))
        self.trayIcon.show()
        self.isEncoding = True
        self.update()

    def cancel(self):
        self.capture_running = False
        self.trayIcon.deleteLater()
        self.cancelled = True
        self.close()
